/*
  Name: Saisamrit Surbehera
  Section : AL
  Date: 21st April

  This is my index.js file. It interactively play the flappy bird Game
	and handle all the events by the user.
*/

(function () {
	"use strict";

  window.addEventListener("load", initialize);

  function initialize() {
		window.addEventListener("keydown", function (e) {
			// space key
			if ([32].indexOf(e.keyCode) > -1) {
				e.preventDefault();
			}
		}, false);
		id("start-btn").addEventListener("click", countDownStart);
	}

  /** This function hides the start button
	 *  And start the countdown, once the user clicks the button
	 */
	function countDownStart() {
		id("start-btn").classList.add("hidden");


    let i = 6;
		let text = document.createElement("p");
		text.setAttribute("id", "countdown");
		id("main").appendChild(text);
		text.innerText = i;
		setTimeout(countDown(), 1000);

		/** This function loops over every single bumber to zero
		and then starts the game at zero
		 */
		function countDown() {
			i--;
			text.innerText = i;
			if (i > 0) {
				setTimeout(countDown, 500);
			}
      else {
				text.remove();
				game();
			}
		}
	}

	// Learnt this from https://www.youtube.com/watch?v=L07i4g-zhDA

	/**
	This function play the game of the user. It manages all the secondary
	funtions of the program such as loading the images, preparing the bird and pipe
	and interactively making pipes and fallping wings
	*/
  function game() {
		var cvs = id("canvas");
		var ctx = cvs.getContext("2d");
		var bird = new Image();
		var background = new Image();
		var base = new Image();
		var pipeNorth = new Image();
		var pipeSouth = new Image();

		//Declaring the characteristics of the pipes
		//Here the gap refers to the gap between the pipes
		var gap = 120;
		var constant;
		var pipe = [];
		pipe[0] = {
		    x : cvs.width ,
		    y : 0
		};

		//Declaring the characteristics of the bird
		var birdX = 10; //Position of the bord in X
		var birdY = 300 //Position of the bord in Y
		var gravity = 1.50; //How quickly it falls

		//Score tracks the the total pipes crossed
		var score = 0;

		loadImages();
		//When the key is pressed the bird flaps and
		// goes up and key is applicable
		document.addEventListener("keydown",flap);


		//Load the images from the directory
		function loadImages(){
			bird.src = "images/bird.png";
			background.src = "images/bg.png";
			base.src = "images/fg.png";
			pipeNorth.src = "images/pipeNorth.png";
			pipeSouth.src = "images/pipeSouth.png";
		}

		//Make the bird flap
		function flap(){
		    birdY -= 30;
		}

		//Print the lost message and start a new game for the user
		function lose() {
			alert("You lost! Your score was " + score);
			location.reload();
		}

		// Draw the backgorund and all the images
		function draw(){

				//Draw the background
		    ctx.drawImage(background,0,0);

		    for(var i = 0; i < pipe.length; i++) {

		        constant = pipeNorth.height + gap;

						//Draw the pipes
		        ctx.drawImage(pipeNorth,pipe[i].x, pipe[i].y);
		        ctx.drawImage(pipeSouth,pipe[i].x, pipe[i].y+constant);

		        pipe[i].x--;

						//Reset a new pipe when the pipe is in the middle of the screen
		        if( pipe[i].x == 125 ){
		            pipe.push({
		                x : cvs.width,
		                y : Math.floor(Math.random()*pipeNorth.height)-pipeNorth.height
		            });
		        }


						//Check if the fallpy bird is following the route
		        if( birdX + bird.width >= pipe[i].x
							&& birdX <= pipe[i].x + pipeNorth.width
							&& (birdY <= pipe[i].y + pipeNorth.height
								|| birdY+bird.height >= pipe[i].y+constant)
								|| birdY + bird.height >=  cvs.height - base.height){
		            lose();
		        }

						//If the bird crosses halfway of the pope add a score
		        if(pipe[i].x == 5){
		            score++;
		        }


		    }

		    ctx.drawImage(base,0,cvs.height - base.height);
		    ctx.drawImage(bird,birdX,birdY);
		    birdY += gravity;

		    ctx.fillStyle = "#000";
		    ctx.font = "30px Verdana";
		    ctx.fillText("Score : "+ score , cvs.width/4 -10, cvs.height-20);
				// Borrowed from https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
		    requestAnimationFrame(draw);

		}
		draw();
	}







	/** This is a helper method to reduce the typing time for getting
	 *  the access to the first element of the tag/class given by accepting
	 *  @param {String} tag - the class/tag of element we're trying to access
   *  @returns {querySelector} of given class/tag
	 */
	function qs(tag) {
		return document.querySelector(tag);
	}

	/** This is a helper method to reduce the typing time for getting
	 *  the access to all elements in the tag/class given by accepting
	 *  @param {String} tag - the class/tag of elements we want to look at
   *  @returns  {querySelectorAll} - array of every element inside the tag/class
	 */
	function qsa(tag) {
		return document.querySelectorAll(tag);
	}


  function id(idName) {
  return document.getElementById(idName);
}


})();

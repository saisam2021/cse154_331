<?php
# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program accepts a two GET parameters one is  'state' representing a all States
# outputting all states and their code. Second parameter is 'data' shows all the
# states and it's capitol . If the paramters arte invalid it shows an 400 error
# message
header("Content-type: text/plain");

if (isset($_GET["state"])) {
  $name = $_GET["state"];
  if($name  === "all"){
    $states  = get_names_all();
  }
  else{
    header("HTTP/1.1 400 Invalid Request");
    echo "Error: Please all as a parameter.";
  }
}
else if( isset($_GET["data"]) ) {
  $name = $_GET["data"];
  if($name  === "true"){
    get_csv();
  }
  else{
      header("HTTP/1.1 400 Invalid Request");
      echo "Error: Please pass all  as a parameter.";
  }
}
else {
  header("HTTP/1.1 400 Invalid Request");
  echo "Error: Please pass a valid parameter.";
}


/**
 * Gets the value of all the states and their coees
 */
function get_names_all(){
     $list_states = file_get_contents("data/states.txt");
     echo $list_states;
}

/**
 * Reads the csv and makes a json file with its capitals
 */
function get_csv(){
  $states = file_get_contents("data/dataStates.csv");
  $states_list=  explode("\n", $states);
  $output = array();
  for( $i =0 ; $i < count($states_list) ; $i++){
    $state_data = explode(",", $states_list[$i]);
    $output[$state_data[0]] = isset($state_data[1]) ? $state_data[1] : null;
  }
  print(json_encode($output));
}
?>

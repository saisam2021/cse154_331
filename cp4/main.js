
/*
  Name: Saisamrit Surbehera
  Section : AL

  This is my index.js file. It  handle all the events by the user.
*/

(function() {
  "use strict";

  /*This is the base url to get the data from php file.*/
  const URL_BASE = "getData.php";
  let replaceSelected;
  /*This keeps track of the replace the user selects.*/

  window.addEventListener("load", initialize);

  /**
   * This runs at the start of the page load.
   * This allows the dropdown to be populated and the button to respond once the user clicks it.
   */
  function initialize() {
    getreplace();
    qs("button").addEventListener("click", getInfo);
  }

  /**
   * This recieves a list of countries and populates the dropdown menu with the all the name of
   * the countries
   */
  function getreplace() {
    let url = URL_BASE + "?state=all";
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(fillDropDown)
      .catch(handleError);
  }

  /**
  * Fills the dropdown menu with the list of restcountrie
  * @param {Object} data - contains the data of countries
  */
  function fillDropDown(data) {
    let dataList = data.split("\n");
    for (let i = 0; i < dataList.length; i++) {
      let newOption = document.createElement("option");
      newOption.value =dataList[i].split("\t")[1];
      newOption.innerText = dataList[i].split("\t")[0];
      qs("select").appendChild(newOption);
    }
  }

  /** Traverses through the whole data set to retrive coutires data
  */
  function getInfo(){
    qs("div").innerHTML="";
    replaceSelected = qs("select").value.toLowerCase();
    let url = URL_BASE +"?data=true";
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(showDetails)
      .catch(handleError);

  }


  /** Finds the flag of the State and search for the capitol
  * @param {Object} dataJSON - contains the data of states
  */
  function showDetails(dataJSON){
    let data = dataJSON[replaceSelected.toUpperCase()];
    let newHeading = document.createElement("h3");
    newHeading.innerText = data;
    let newReplace = document.createElement("div");
    let cardImg  = document.createElement("img");
    cardImg.src = "data/flags/"+replaceSelected +".png";
    cardImg.alt = "Flag";
    newReplace.appendChild(cardImg);
    qs("div").appendChild(newHeading).appendChild(newReplace);
  }


  /**
   * This lets the user know when an error occured during the attempt to recieve the list of
   * states
   */
  function handleError() {
    let message = document.createElement("p");
    message.innerText = "Due To Global warming, The place has disappeared";
    qs("fieldset").appendChild(message);
  }

  /* ------------------------------ Helper Functions (from slide) ------------------------------ */

  /**
   * Returns the first element that matches the given CSS selector.
   * @param {string} query - CSS query selector.
   * @returns {object} The first DOM object matching the query.
   */
  function qs(query) {
    return document.querySelector(query);
  }

  /**
   * Helper function to return the response's result text if successful, otherwise
   * returns the rejected Promise result with an error status and corresponding text
   * @param {object} response - response to check for success/error
   * @returns {object} - valid result text if response was successful, otherwise rejected
   *                     Promise result
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300 || response.status == 0) {
      return response.text();
    } else {
      return Promise.reject(new Error(response.status + ": " + response.statusText));
    }
  }
})();

/*
  Name: Saisamrit Surbehera
  Section : AL

  This is my bestreads.js file. It  handle all the events by the user.
*/

(function() {
  "use strict";

  /*This is the base url to get the data from php file.*/
  const URL_BASE = "bestreads.php";
  let currentBook;
  /*This keeps track of the replace the user selects.*/

  window.addEventListener("load", initialize);

  /**
   * This runs at the start of the page load.
   * This allows the dropdown to be populated and the button to respond once the user clicks it.
   */
  function initialize() {
    getListView("nothing");
    id("home").addEventListener("click", goHome);
    id("search-btn").addEventListener("click" , searchBook);
    id("back").addEventListener("click", goStart);
  }

  /**
   * The funtion search for the book and replaces the list view with the searched
   * term
   */
  function searchBook(){
    if(id("search-term").value.trim() === ""){
      getListView("nothing");
    }
    else{
      getListView(id("search-term").value.trim());
    }
    id("home").disabled = false;
    goStart();
  }

  /**
  * This function add the error text if there is no search
  */
  function getErrorText1(){
    id("error-text").innerText = "No books found that match the search string "+
      id("search-term").value+" , please try again.";

    id("error-text").classList.remove("hidden");
    id("home").disabled = false;
  }

  /**
  * This function add the error text if there is error
  */
  function getErrorText2(){
    id("error-text").innerText =  "Something went wrong with the request. Please"+
     "try again later.";
    id("error-text").classList.remove("hidden");
    id("home").disabled = false;
  }

  /**
  * This function changes the attributes of page when called
  */
  function  goStart(){
    id("error-text").classList.add("hidden");
    id("single-book").classList.add("hidden");
    id("book-list").classList.remove("hidden");
    id("back").classList.remove("hidden");
    id("back").classList.add("hidden");
  }

  /**
  * The function changes the attriburte of the page when Home is clicked
  */
  function goHome(){
    id("search-term").value = '';
    getListView("nothing");
    goStart();
    id("home").disabled = true;

  }

  /**
  * The function changes the attriburte of the page to enable the home button
  */
  function enableHome(){
    id("back").classList.remove("hidden");
    id("home").disabled = false;
  }

  /**
  * The funion updates the list view depending on the paramrtetr passed
  * @param {String} title : the title of the page otherwise "nothing"
  */
  function getListView(title){
    let url = URL_BASE + "?mode=books";
    if(!(title === "nothing")){
      url = url + "&search="+title;
    }
    id("book-list").innerHTML= '';
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(fillListView)
      .catch(handleError);
  }

  /**
  * Fills the book list when the page is filled
  * @param {Object} data - contains the data of all the books
  */
  function fillListView(data) {
    if(data.books.length === 0){
      getErrorText1();
    }
    for (let i = 0; i < data.books.length; i++) {
      let newCard = document.createElement("div");
      newCard.setAttribute("id", data.books[i].folder);

      let title = data.books[i].title;
      let cardTextNode = document.createElement("p");
      cardTextNode.innerText = title;

      let newCardImage = document.createElement("img");
      newCardImage.src = "books/" + data.books[i].folder + "/cover.jpg";
      newCardImage.alt = title;

      newCard.appendChild(newCardImage);
      newCard.appendChild(cardTextNode);
      newCard.addEventListener("click",clickCard);
      id("book-list").appendChild(newCard);
    }
  }

  /**
  * Fills the information and replaces the view wehen a card is clicked
  */
  function clickCard(){
    id("back").classList.remove("hidden");
    id("book-list").classList.add("hidden");
    id("single-book").classList.remove("hidden");
    currentBook = this.id;
    id("book-cover").src = "books/" + currentBook + "/cover.jpg";
    enableHome();
    addInfo();
    addDescription();
    addReview();
  }

  /**
  * Fills the information of the book
  */
  function addInfo(){
    let url = URL_BASE + "?mode=info&title="+currentBook;
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(getInfo)
      .catch(handleError);
  }


  /**
  * Fills the information of the book
  * @param {Object} data - contains the info data of the book
  */
  function getInfo(data){
    id("book-title").innerText = data.title;
    id("book-author").innerText = data.author;
  }

  /**
  * Fills the description of the book
  */
  function addDescription(){
    let url = URL_BASE + "?mode=description&title="+currentBook;
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(getDescription)
      .catch(handleError);
  }

  /**
  * Fills the description of the book
  * @param {Object} data - contains the description data of the book
  */
  function getDescription(data){
    id("book-description").innerText = data;
  }

  /**
  * Adds all the reviews
  */
  function addReview(){
    id("book-reviews").innerHTML = '';
    let url = URL_BASE + "?mode=reviews&title="+currentBook;
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(getReview)
      .catch(handleError);
  }

  /**
  * Fills the reviews_list with the reviews_list
  * @param {Object} data - contains the review data of the book
  */
  function getReview(data){
    let totalScore =0;

    for(let i =0; i< data.length; i++){
      let name = document.createElement("h3");
      name.innerText = data[i].name;

      let rating = document.createElement("h4");
      rating.innerText = "Rating: "+parseFloat(data[i].rating);
      totalScore = totalScore+ parseFloat(data[i].rating);

      let review = document.createElement("p");
      review.innerText = data[i].text;
      id("book-reviews").appendChild(name);
      id("book-reviews").appendChild(rating);
      id("book-reviews").appendChild(review);
    }
    id("book-rating").innerText = (totalScore/data.length).toFixed(1);
  }



  /**
   * This lets the user know when an error occured during the attempt to recieve the list of
   * states
   */
  function handleError() {
    getErrorText2();
  }

  /* ------------------------------ Helper Functions (from slide) ------------------------------ */


  /** This is a helper method to reduce the typing time for getting
  	*  the access to ElementId by accepting
  	*  @param {String} idName - the id of element we're trying to access
  	*  @returns {ElementId} of given id
  */
  function id(idName) {
    return document.getElementById(idName);
	}

  /**
   * Helper function to return the response's result text if successful, otherwise
   * returns the rejected Promise result with an error status and corresponding text
   * @param {object} response - response to check for success/error
   * @returns {object} - valid result text if response was successful, otherwise rejected
   *                     Promise result
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300 || response.status == 0) {
      return response.text();
    } else {
      return Promise.reject(new Error(response.status + ": " + response.statusText));
    }
  }
})();

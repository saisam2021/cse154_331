<?php

# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program accepts a four GET parameters :
# info : return the information for the title
# reviews : return the reviews for the title
# description : return the description for the title
# books : return the list of all books

header("Content-type: text/plain");

#This handles all the parameter, their expected output and their expected error
if (isset($_GET["mode"])) {
  if($_GET["mode"] === "books"){
     get_titles_and_location();
  }
  else if($_GET["mode"] === "description"){
    if(isset($_GET["title"])){
      if(file_exists("books/".$_GET["title"]."/info.txt")){
        echo file_get_contents("books/".$_GET["title"]."/description.txt");

      }
      else{
        header("HTTP/1.1 400 Invalid Request");
        echo "No description for ".$_GET["title"]." was found.";
      }
    }
    else{
      header("HTTP/1.1 400 Invalid Request");
      echo "Please remember to add the title parameter when using mode=description.";
    }
  }
  else if($_GET["mode"] === "info"){
    if(isset($_GET["title"])){
      if(file_exists("books/".$_GET["title"]."/info.txt")){
        $info = explode("\n",file_get_contents("books/".$_GET["title"]."/info.txt"));
        print(json_encode(array("title" => $info[0], "author" => $info[1] )));
      }
      else{
        header("HTTP/1.1 400 Invalid Request");
        echo "No info for ".$_GET["title"]." was found.";
      }
    }
    else{
      header("HTTP/1.1 400 Invalid Request");
      echo "Please remember to add the title parameter when using mode=info.";
    }
  }
  else if($_GET["mode"] === "reviews"){
    if(isset($_GET["title"])){
      get_reviews($_GET["title"]);
    }
    else{
      header("HTTP/1.1 400 Invalid Request");
      echo "Please remember to add the title parameter when using mode=reviews.";
    }
  }
  else{
    header("HTTP/1.1 400 Invalid Request");
      echo "Please provide a mode of description, info, reviews, or books.";
    }
}

else{
  header("HTTP/1.1 400 Invalid Request");
  echo "Please provide a mode of description, info, reviews, or books.";
}


/*
Handles the reviews and print all of revies in JSON Format
@param {String} $title: the title of the book
*/
function get_reviews($title){
    $reviews = glob('books/*'.$title.'/review*.txt');
    if(count($reviews)  === 0){
      header("HTTP/1.1 400 Invalid Request");
      echo "No reviews for ".$title." was found.";
    }
    $result = array();
    foreach($reviews  as $review_file){
      $review_data = explode("\n",file_get_contents($review_file));;
      array_push($result, Array("name"=> $review_data[0] , "rating" => $review_data[1], "text" => $review_data[2]));
    }
    print(json_encode($result));
}

/*
Handles the reviews and print all of titles and folders in json format
*/
function get_titles_and_location(){
  $locations;
  if(isset($_GET["search"])) {
    $locations = get_matching_titles($_GET["search"]) ;
  }
  else{
    $locations = scandir('books/');
    $locations = array_slice ( $locations ,3);
  }
  $books_list = array();
  foreach( $locations as $book_name ){
    $list_title = get_first_info("books/" . $book_name."/info.txt");
    array_push($books_list, array('title' => $list_title , 'folder' =>
    $book_name));
  }
  print(json_encode(array('books' => $books_list )));
}


/*
Finds matching titles to all $search query
@param {Object} $search: the search to query
@return {Array} $search_array: all the folders with maching arrays
*/
function get_matching_titles($search){
  $locations = scandir('books/');
  $locations = array_slice ( $locations ,3);
  $search_array = array();
  foreach( $locations as $book_name ){
    if (!(strpos(get_first_info("books/" . $book_name."/info.txt") , $search) === false)) {
      array_push($search_array,$book_name);
    }
  }
  return $search_array;
}



/*
Finds the content of the file
@param {Array} $path: the file given
@return {String} : all the folders with maching arrays
*/
function get_first_info($path) {
  if(file_exists($path)){

      $text = file_get_contents($path);
      return explode("\n",$text)[0];
  }
  echo "file doesn't exist";
  return "";
}

?>

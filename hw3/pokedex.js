(function() {
  "use strict";

  /**
  * This is the base url to get the data from pokedex.
  * @constant
  * @type {String}
  */
  const URL_BASE_POKEDEX = "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/";
  /**
  * This is the base url to play the gamne from pokedex API.
  * @constant
  * @type {String}
  */
  const URL_GAME = "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/game.php";
  /**
  * This is the base Pokemon list
  * @constant
  * @type {String[]}
  */
  const BASE_POKEMON = ["bulbasaur", "charmander", "squirtle"];

  /**
  * This is the base Pokemon Data
  * @type {Object}
  */
  let currentPokemon;
  /**
  * This is Game UID
  * @type {String}
  */
  let guid;
  /**
  * This is Game Player ID
  * @type {String}
  */
  let pid;

  window.addEventListener("load", initialize);

  /**
   * This runs at the start of the page load.
   * This allows the Pokedex to be filled with the all the pokemons
   */
  function initialize() {
    getPokedexPics();
    id("endgame").addEventListener("click", addEndGame);
    id("flee-btn").addEventListener("click", fleeTheBattle);
  }

  /**
  * Flees the battle, changes the characteristics of the card and sends flee message
  * to the API
  */
  function fleeTheBattle(){
    qs("#loading").classList.add("hidden");
    let params =  new FormData();
    params.append("guid", guid);
    params.append("pid", pid);
    params.append("movename", "flee");
    fetch(URL_GAME, {method: "POST", body: params})
      .then(checkStatus)
      .then(JSON.parse)
      .then(updateGameData)
      .catch(console.error);
  }

  /**
  * When "Back to Pokedex" is clicked change the characteristics of the whole pokdex
  */
  function addEndGame() {
    qs("#p1 .health-bar").classList.remove("low-health");
    qs("#p2 .health-bar").classList.remove("low-health");
    qs("#p1 .health-bar").style.width = "100%";
    qs("#p2 .health-bar").style.width = "100%";
    clearBuffs();
    qs("h1").innerText = "Your Pokedex";
    qs("#p1 .hp").innerText = currentPokemon.hp+"HP";
    id("start-btn").classList.remove("hidden");
    id("pokedex-view").classList.remove("hidden");
    id("p2").classList.add("hidden");
    qs(".hp-info").classList.add("hidden");
    id("results-container").classList.add("hidden");
    id("flee-btn").classList.add("hidden");
    qs(".buffs").classList.add("hidden");
    id("endgame").classList.add("hidden");
  }


  /**
   * This recieves the list of all the pokemon  and populates the pokedex view
   */
  function getPokedexPics(){
    let url = URL_BASE_POKEDEX + "pokedex.php?pokedex=all";
    fetch( url , {mode : "cors"})
      .then(checkStatus)
      .then(fillPokedex)
      .catch(console.error);
  }


  /**
  * Fills the pokedex view with the all pokemons
  * @param {String} dataRaw - contains the list of all pokemon
  */
  function fillPokedex(dataRaw) {
    let data = dataRaw.split("\n");
    for (let i = 0; i < data.length; i++) {
        let dataSplit = data[i].split(":");
        let cardImg  = document.createElement("img");
        cardImg.classList.add("sprite");
        cardImg.src = URL_BASE_POKEDEX + "sprites/" + dataSplit[1] + ".png";
        cardImg.alt = "Pokemon : " + dataSplit[0];

        cardImg.setAttribute("id", dataSplit[1]);

        id("pokedex-view").appendChild(cardImg);
    }
    fillBase();
  }



  /**
  * Enables the base pokemon to be found and given properties
  */
  function fillBase() {
    for( let i = 0; i < BASE_POKEMON.length; i++){
      fillFoundByName(BASE_POKEMON[i]);
    }
  }

  /**
  * Fills the pokedex view with the all pokemons
  * @param {String} name - contains the name of the pokemon
  */
  function fillFoundByName(name){
    id(name).classList.add("found");
    id(name).addEventListener("click", addCardFeaures);
  }

  /**
  * Get all the properties of a selected pokemon and  update the card
  */
  function addCardFeaures(){
    let url = URL_BASE_POKEDEX + "pokedex.php?pokemon=" + this.id;
    fetch( url , {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(changeCardP1)
      .catch(console.error);
    id("start-btn").classList.remove("hidden");
    id("start-btn").addEventListener("click", startGame );
  }

  /**
  * Starts the game by removing the pokedex and adding the opponent card
  */
  function startGame(){
    id("endgame").classList.add("hidden");
    id("start-btn").classList.add("hidden");
    id("pokedex-view").classList.add("hidden");
    id("p2").classList.remove("hidden");
    qs(".hp-info").classList.remove("hidden");
    id("results-container").classList.remove("hidden");
    id("flee-btn").classList.remove("hidden");
    qs(".buffs").classList.remove("hidden");
    let buttons = qsa("button");
    for( let i = 0; i< 4; i++){
      buttons[i].disabled = false;
    }
    qs("h1").innerText = "Pokemon Battle Mode!";
    qs("#p1-turn-results").innerText = "";
    qs("#p2-turn-results").innerText = "";
    let params =  new FormData();
    params.append("startgame", "true");
    params.append("mypokemon", currentPokemon.shortname);
    fetch(URL_GAME, {method: "POST", body: params})
      .then(checkStatus)
      .then(JSON.parse)
      .then(getGameData)
      .catch(console.error);
    addButtonFeatures();
  }

  /**
  * Adds event listeners to all buttons
  */
  function addButtonFeatures(){
    let allButoonsP1 = qsa("#p1 .moves > button");
    for(let i=0; i < allButoonsP1.length; i++){
      allButoonsP1[i].addEventListener("click", move);
    }
  }

  /**
  * When a button is clicked for the move interacts with the game api
  * and change the game characteristics
  */
  function move(){
    qs("#loading").classList.remove("hidden");
    let params =  new FormData();
    params.append("guid", guid);
    params.append("pid", pid);
    params.append("movename", lowerTrim(this.children[0].innerText));
    fetch(URL_GAME, {method: "POST", body: params})
      .then(checkStatus)
      .then(JSON.parse)
      .then(updateGameData)
      .catch(console.error);
  }

  /**
  * Interacts with the api and changes whole game
  * @param {Object} data - contains the data of game and its results
  */
  function updateGameData(data){
    qs("#p1-turn-results").classList.remove("hidden");
    qs("#p2-turn-results").classList.remove("hidden");
    qs("#loading").classList.add("hidden");
    qs("#p1-turn-results").innerText = "Player 1 played " + data.results["p1-move"] +
    "  and it " + data.results["p1-result"];
    if(data.results["p2-move"] === null &&  data.results["p2-result"] === null){
      if(data.results["p1-move"].toLowerCase() === "flee"){
        endGameLost();
        qs("#p2-turn-results").innerText = "";
      }
      else{
        qs("h1").innerText = "You Won!";
        fillFoundByName(data.p2.shortname);
        disableButtons();
        id("flee-btn").classList.add("hidden");
        id("endgame").classList.remove("hidden");
      }
    }
    else{
      qs("#p2-turn-results").innerText = "Player 2 played " + data.results["p2-move"] +
      "  and it " + data.results["p2-result"];
    }

    if(data.p1["current-hp"] <= 0){
      endGameLost();
    }

    updateHealthBar(data);
    clearBuffs();

    //buffs for p1
    let buffs = data.p1.buffs;
    let debuffs = data.p1.debuffs;
    addBuffFeactures("#p1" , "buff" , buffs);
    addBuffFeactures("#p1" , "debuff" , debuffs);
    //buffs for p2
    let buffsP2 = data.p2.buffs;
    let debuffsP2 = data.p2.debuffs;

    addBuffFeactures("#p2" , "buff" , buffsP2);
    addBuffFeactures("#p2" , "debuff" , debuffsP2);
  }

  /**
  * We the user losses changes the heading to You lost and change all the properties
  */
  function endGameLost(){
    disableButtons();
    qs("h1").innerText = "You Lost!";
    id("flee-btn").classList.add("hidden");
    id("endgame").classList.remove("hidden");
  }

  /**
  * Disables the buttons
  */
  function disableButtons() {
    let a = qsa(".card button");
    for(let i =0; i < a.length; i++){
      a[i].disabled= true;
    }
  }

  /**
  * Updates the Health Bar and changes the health bar every time it is called
  * @param {Object} data : Contains all he data of the game
  */
  function updateHealthBar(data){
    //Update the Health bar
    let calculateHealthP1 = data.p1["current-hp"] *100 / data.p1.hp;
    let calculateHealthP2 = data.p2["current-hp"] *100 / data.p2.hp;
    qs("#p1 .health-bar").style.width= Math.floor(calculateHealthP1)+"%";
    qs("#p2 .health-bar").style.width= Math.floor(calculateHealthP2)+"%";

    if(calculateHealthP1 <= 20){
      qs("#p1 .health-bar").classList.add("low-health");
    }
    if(calculateHealthP2 <= 20){
      qs("#p2 .health-bar").classList.add("low-health");
    }
    qs("#p1 .hp").innerText = data.p1["current-hp"] + "HP";
    qs("#p2 .hp").innerText = data.p2["current-hp"] + "HP";
  }

  /**
  * Clears the buffs columns
  */
  function clearBuffs() {
    let currentBuffsP1 =  qs("#p1 .buffs");
    let currentBuffsP2 = qs("#p2 .buffs");
    while (currentBuffsP1.firstChild) {
      currentBuffsP1.removeChild(currentBuffsP1.firstChild);
    }
    while (currentBuffsP2.firstChild) {
      currentBuffsP2.removeChild(currentBuffsP2.firstChild);
    }
  }


  /**
  * Adds features to all the buffs
  * @param {String} card - contains the sstring of whoch card to change
  * @param {String} buff - adds the buff or the debuff
  * @param {String[]} buffList -list of all the buffs
  */
  function addBuffFeactures(card,buff, buffList){
    for(let i =0; i< buffList.length; i++){
      let a = createElement("div");
      a.classList.add(buff);
      a.classList.add(buffList[i]);
      qs(card + " .buffs").appendChild(a);
    }
  }

  /**
  * take a string and return a string with whitespcae removed with all lowercase
  * @param {String} string : the string provided by the user
  * @returns {String} resuting trimmed with whitespace and lowercase
  */
  function lowerTrim( string) {
    return string.toLowerCase().replace(/\s+/g, '');
  }

  /**
  * Adds features to all the Card of P2
  * @param {String} data: the data of the pokemon
  */
  function getGameData(data){
    cardChange("#p2",data.p2);
    guid = data.guid;
    pid = data.pid;
  }

  /**
  * Adds features to all the Card of P1
  * @param {String} data: the data of the pokemon
  */
  function changeCardP1( data){
    currentPokemon = data;
    cardChange("#p1",data);
  }


  /**
  * Gives the card all the properties
  * @param {Object} card - contains the sstring of whoch card to change
  * @param {Object} data - contains the data of pokemon
  */
  function cardChange(card,data){
    qs(card+" .name").innerText = data.name;
    qs(card+" .info").innerText = data.info.description;
    qs(card+" .hp").innerText = data.hp + "HP";
    qs(card+" .pokepic").src = URL_BASE_POKEDEX + data.images.photo;
    qs(card+" .pokepic").alt = data.name + " Picture";
    qs(card+" .type").src = URL_BASE_POKEDEX + data.images.typeIcon;
    qs(card+" .type").alt = "type image";
    qs(card+" .weakness").src = URL_BASE_POKEDEX + data.images.weaknessIcon;
    qs(card+" .weakness").alt = "weakness image";
    let dataMoves = data.moves;
    for( let i = 0; i< 4; i ++){
      if(i  < dataMoves.length ){
        qsa(card + " button")[i].classList.remove("hidden");
        qsa(card + " .move ")[i].innerText = dataMoves[i].name;
        if(!(dataMoves[i].dp === undefined)){
          qsa(card + " .dp ")[i].innerText = dataMoves[i].dp + "DP";
        }
        else{
          qsa( card + " .dp ")[i].innerText = "";
        }
        qsa(card+ " button img")[i].src = URL_BASE_POKEDEX + "icons/" + dataMoves[i].type
        + ".jpg";
      }else{
        qsa(card+ " button")[i].classList.add("hidden");
      }
    }
  }




  /* ------------------------------ Helper Functions (from slide) ------------------------------ */


  /** This is a helper method to reduce the typing time for getting
  	*  the access to ElementId by accepting
  	*  @param {String} idName - the id of element we're trying to access
  	*  @returns {ElementId} of given id
  */
  function id(idName) {
    return document.getElementById(idName);
	}


  /**
   * Returns the first element that matches the given CSS selector.
   * @param {string} query - CSS query selector.
   * @returns {object} The first DOM object matching the query.
   */
  function qs(query) {
    return document.querySelector(query);
  }

  /**
   * Return a lst of documents elements that match the specified selector
   * @param {string} query - CSS query selector.
   * @returns { NodeList}  representing a list of the document's elements that
   * match the specified group of selectors.
   */
  function qsa(query) {
    return document.querySelectorAll(query);
  }

  /**
   * Return a new element with the properties of query
   * @param {string} query - a string with the query.
   * @returns { NodeList}  representing a new element that
   * match the specified query
   */
  function createElement(query){
    return document.createElement(query);
  }

  /**
   * Helper function to return the response's result text if successful, otherwise
   * returns the rejected Promise result with an error status and corresponding text
   * @param {object} response - response to check for success/error
   * @returns {object} - valid result text if response was successful, otherwise rejected
   *                     Promise result
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300 || response.status == 0) {
      return response.text();
    } else {
      return Promise.reject(new Error(response.status + ": " + response.statusText));
    }
  }

  })();

(function () {
	"use strict";

	/* Decalaring all the constants */
	/** @constant
	@type {Object}
	*/
	const STYLE = ["outline", "striped" ,"solid"];
	const COLOUR = ["green" , "purple" , "red"];
	const SHAPES = ["diamond", "oval" , "squiggle"];
	/** @constant
	@type {number}
	*/
	let countSet = 0;
	let time = 0;
	/** @constant
	*/
	let timerID;

	/** This command load every functions declared in this JS file
	*  to run in the background of the website
	*/
	window.addEventListener("load", initialize);

	/** This function initialize the state of the page
	*  enabling the user to click all the buttons in the pages
	*  and tying them to their respective functions
	*/
	function initialize() {
		id("start").addEventListener("click", startGame);
		id("refresh").addEventListener("click", clickRefresh);
		id("main-btn").addEventListener("click" , clickMain);
	}


	/** This function hides the menu view and shows the game view while aldding all
	*   its feactures
	*/
	function startGame() {
		id("menu-view").classList.add("hidden");
		id("game-view").classList.remove("hidden");
		addCards();
		selectedCards();
		timerUpdate();
	}

	/** This function updates the timer periodically based upon the user selectecd options
	command
	*/
	function timerUpdate(){
		let timeGiven = qs("select").value;
		timerID  = id("time");
		if (timeGiven == "none") {
			time = 0;
			timerID = setInterval(function() {
				time++;
				updateTime();
			},1000);
		}
		else {
			time = timeGiven;
			updateTime();
			timerID = setInterval( function() {
				time--;
				updateTime();
				if(time <= 0 ){
					time = 0;
					updateTime();
					clearTimeout(timerID);
					id("refresh").removeEventListener("click",clickRefresh);
					removeShadow();
					removeSelect();
				}
			}, 1000);
		}
	}

	/** This function hides removes all the click from the cards
	*/
	function removeSelect(){
		let x = document.getElementsByClassName("card");
		for (let i = 0; i < x.length; i++) {
			console.log(x[i]);
			x[i].removeEventListener("click", selectCard );
		}
	}

	/** This function hides removes all the shadow from the cards
	*/
	function selectCard(){
		this.classList.toggle("selectedCard");
		select3();
	}


	/** This function updates the timer based upon the state of the game
	*/
	function updateTime() {
		let minutes = Math.floor(time / 60);
		let seconds = time % 60;
		if(minutes <= 9){
			minutes = "0" + minutes;
		}
		if(seconds <= 9 ){
			seconds = "0" + seconds;
		}
		id("time").innerText = "" + minutes + ":" + seconds;
	}

	/** This function check and runs an operation if there are three checked
	*   cards
	*/
	function select3(){
		let x = classElement("selectedCard");
		if(x.length == 3 ){
			evaluateSet();
		}
	}

	/** This function evaluates the selected cards tries to find if they satisfy
	* the condition and display the relvant messages
	*  on the cards
	*/
	function evaluateSet(){
		let selectedCards = classElement("selectedCard");
		let parentNode = id("game");
		let card1 = selectedCards[0];
		let card2 = selectedCards[1];
		let card3 = selectedCards[2];
		if(isSet(selectedCards) ){
			replaceWith(parentNode, card1);
			replaceWith(parentNode, card2);
			replaceWith(parentNode, card3);
			countSet++;
			changeSetCounter();
		}
		else{
			replaceWithNSET(parentNode, card1);
			replaceWithNSET(parentNode, card2);
			replaceWithNSET(parentNode, card3);
			removeShadow();
			if (qs("select").value == "none") {
				time= time + 15;
			}
			else{
				if(time<16 ){
					time =0;

				}
				else{
					time = time - 15;
				}
			}
			updateTime();
		}
	}

	/** This function replaces the card with the text card and replaces it with a new
	* card picture
	* @param {Element}parentNode the parent Node of the card
	* @param {Element}card The selected card to be replaced
	*/
	function replaceWith(parentNode, card){
		let nodeN = createTextNode("Set!");
		parentNode.replaceChild( nodeN , card);
		setTimeout(function(){
			let cardReplaced = returnUniqueCard();
			cardReplaced.addEventListener("click", function(){
				this.classList.toggle("selectedCard");
				select3();
			});
			parentNode.replaceChild(cardReplaced, nodeN );
		}, 1000);
	}

	/** This function replaces the card with the text card and replaces it back to
	* the normal card
	* @param {Element}parentNode :the parent Node of the card
	* @param {Element}card :The selected card to be replaced
	*/
	function replaceWithNSET(parentNode, card){
		let nodeN = create("div");
		nodeN.setAttribute("class", "card");
		nodeN.appendChild(document.createTextNode("Not a Set"));
		parentNode.replaceChild( nodeN , card);
		let cloneCard = card.cloneNode(true);
		setTimeout(function(){
			parentNode.replaceChild(cloneCard , nodeN );
			cloneCard.addEventListener("click", selectCard);
			cloneCard.classList.toggle("selectedCard");
			if(time ==0){
				removeSelect();
			}
		}, 1000);
	}

	/** This function creates a new text code and returns it
	*		@param {String}text :the text of the message to be displayed on the card
	*		@return {Element}nodeN: returns the completed text node
	*/
	function createTextNode(text){
		let nodeN = create("div");
		nodeN.setAttribute("class", "card");
		nodeN.appendChild(document.createTextNode(text));
		return nodeN;
	}

	/** This function replaces the card with the text card and eplaces it with
	*		the preivious card
	*		@param {String} text :the text of the message to be displayed on the card
	*/
	function removeShadow(){
		let x = classElement("selectedCard");
		let n = x.length;
		for(let i = 0;  i < n; i++){
			x[0].classList.toggle("selectedCard");
		}
	}

	/** This function replaces the card with the numer of sets
	*/
	function changeSetCounter(){
		id("set-count").innerText = countSet;
	}

	/** Return if the set is valid
	* @param {HTMLCollection} selectedCards : All the selcted addCards
	* @returns {boolean}  True if it a set otherwise false
	*/
	function isSet(selectedCards){
		let card1ID = selectedCards[0].id.split("-");
		let card2ID = selectedCards[1].id.split("-");
		let card3ID = selectedCards[2].id.split("-");
		let card12c = card1ID.filter(value => -1 !== card2ID.indexOf(value));
		let card13c = card2ID.filter(value => -1 !== card3ID.indexOf(value));
		let card23c = card2ID.filter(value => -1 !== card3ID.indexOf(value));
		return (card12c.lenght = card13c.length)  && (card12c.lenght = card23c.length) &&  (card12c.length == card23c.length);
	}

	/** Returns if the arrays are equal
	* @param {String[]}	a : Array a
	* @param {String[]} b : Array b
	* @return {boolean} : True is the set is equal otherwise false
	*/
	function arraysEqual(a, b) {
		if (a === b) {return true;}
		if (a == null || b == null){ return false;}
		if (a.length != b.length) {return false;}
		for (let i = 0; i < a.length; ++i) {
			if (a[i] !== b[i]) {return false;}
		}
		return true;
	}

	/** Add the click option on all cards
	*/
	function selectedCards(){
		let x = document.getElementsByClassName("card");
		for (let i = 0; i < x.length; i++) {
			x[i].addEventListener("click", selectCard);
		}
	}

	/** Add the functionality to the click button
	* When the button is clicked it removes the gamefile resets it shows the menu
	*/
	function clickMain(){
		id("game").innerHTML = '';
		id("menu-view").classList.remove("hidden");
		id("game-view").classList.add("hidden");
		countSet = 0;
		changeSetCounter();
		clearTimeout(timerID);
	}

	/** Add the functionality to the refresh button
	* When the button is clicked it resets the board and gives new addCards
	*/
	function clickRefresh(){
		id("game").innerHTML = '';
		addCards();
		selectedCards();
	}

	/** Adds the cards to the board based upon the checked option
	*/
	function addCards(){
		let numberCards= 12;
		if(qs("input").checked){
			numberCards= 9;
		}
		for( let i =0; i<numberCards; i++ ){
			returnUniqueCard();
		}
	}

	/** Make and Returns an unique card different from all  present on the board
	* @return {Element} node : new node with a unique set of characteristics
	*/
	function returnUniqueCard(){
		let generatedCard = generator();
		let numberGen = randGen();
		while(id(generatedCard + "-" + (numberGen+1) ) != null ){
			generatedCard = generator();
			numberGen = randGen();
		}
		let node = create("div");
		node.setAttribute("class", "card");
		node.setAttribute("id", generatedCard + "-" + (numberGen+1) );
		id("game").appendChild(node);
		let cardImg;
		for( let j =-1; j < numberGen; j ++){
			cardImg  = create("img");
			cardImg.src = "img/"+generatedCard+".png";
			node.appendChild(cardImg);
		}
		return node;
	}

	/** A function which Depending on the choice by the user returns a random
	* combination of image characteristics
	* @returns {String} string of the image generated
	*/
	function generator(){
		if(qs("input").checked){
			return "solid" + randomPicGenerator();
		}
		return STYLE[randGen()] + randomPicGenerator();
	}

	/** A function which Choose shape and colour attribute
	* @returns {String} : shape-color of the image
	*/
	function randomPicGenerator(){
		return  "-" + SHAPES[randGen() ] + "-" + COLOUR[randGen()];
	}

	/** Generates a random number in {1,2,3}
	* @returns {number} : random number 1-3
	*/
	function randGen() {
		return Math.floor(Math.random() * (3));
	}

	/** This is a helper method to reduce the typing time for getting
	*  the access to ElementId by accepting
	*  @param {String} idName - the id of element we're trying to access
	*  @returns {ElementId} of given id
	*/
	function id(idName) {
		return document.getElementById(idName);
	}

	/** This is a helper method to reduce the typing time for creating an Element
	*  @param {String} type - the type of element we're trying to generate
	*  @returns {Element} - created element
	*/
	function create(type){
		return document.createElement(type);
	}

	/** This is a helper method to reduce the typing time for getting
	*  the access to elements of a particular type type
	*  @param {String} type - the type of element we're trying to access
	*  @returns {getElementsByClassName} set of elements
	*/
	function	classElement(type){
		return  document.getElementsByClassName(type);
	}

	/** This is a helper method to reduce the typing time for getting
	*  the access to the first element of the tag/class given by accepting
	*  @param {String} tag - the class/tag of element we're trying to access
	*  @returns {querySelector} of given class/tag
	*/
	function qs(tag) {
		return document.querySelector(tag);
	}

})();

<?php

header("Content-type: text/plain");
include 'common.php';

$db = get_PDO();

if( isset($_POST["name"])  ) {
    $result = already_exists($_POST["name"]);
    if($result){
        

        $name = strtolower ( $_POST["name"]);
        $nickname = strtoupper($name);
        if(isset($_POST["nickname"])){
            $nickname = $_POST["nickname"];
        }
    
        $sql = "UPDATE Pokedex
                SET nickname = :nickname
                WHERE name= :name ;";
        $stmt = $db->prepare($sql);
        $params = array(
                    "nickname" => $nickname , "name" => $name);
        $stmt->execute($params);

        $sucess =  "Your ".$_POST["name"]." is now named ".$nickname."!";
        print(json_encode(array('success' => $sucess  )));
    


    }
    else{
        $error = ($_POST["name"]).' not found in your Pokedex.';
        $error = error_msg($error);
    }
}
else{
    $error = ('Missing name parameter.');
    $error = error_msg($error);
}


?>
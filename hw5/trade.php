<?php

# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program returns the message whether one has beentraded or not
# it take in 2 parameters one is mypokemon and thierpokemon
# it removes mypokemon and add theirpokemon

include 'common.php';

header("Content-type: text/plain");

$db = get_PDO();



if( isset($_POST["mypokemon"])  && isset($_POST["theirpokemon"]) ) {
    $result = already_exists($_POST["mypokemon"]);
    if($result){

        //remove our pokemon
        $name = strtolower($_POST["mypokemon"]);
        $stmt = $db->prepare("DELETE FROM Pokedex WHERE name=?");
        $stmt->execute([$name]); 

        //Add their pokemon
        $name = strtolower ( $_POST["theirpokemon"]);
        $nickname = strtoupper($name);
        date_default_timezone_set('America/Los_Angeles');
        $time = date('y-m-d H:i:s');
        try {
            $sql = "INSERT INTO Pokedex(name, nickname,  datefound) "
                . "VALUES (:name, :nickname, :datefound);";
            $stmt = $db->prepare($sql);
            $params = array("name" => $name,
                        "nickname" => $nickname,
                        "datefound" => $time );
            $stmt->execute($params);
            $sucess =  "You have traded your ".$_POST["mypokemon"]." for ".$_POST["theirpokemon"]."!";
            print(json_encode(array('success' => $sucess  )));
        }
        catch (PDOException $ex)  {
            $error = "You have already found ".($_POST["theirpokemon"].".");


            $error = error_msg($error);
            
        }

    }
    else{
        $error = ($_POST["mypokemon"]).' not found in your Pokedex.';

        $error = error_msg($error);
    }
}
else if(!(isset($_POST["mypokemon"]))  && (isset($_POST["theirpokemon"] ))){
    $error = ('Missing mypokemon parameter.');
    $error = error_msg($error);

}
else if(!(isset($_POST["theirpokemon"])) && (isset($_POST["mypokemon"]) )) {
    $error = ('Missing theirpokemon parameter.');
    $error = error_msg($error);
}
else{
    $error = ('Missing mypokemon and theirpokemon parameter.');
    $error = error_msg($error);
}










?>
<?php
include 'common.php';
# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program returns the message whether one has been added or not
# it take in 2 parameters one is name and the nickname
# It adds it to the list


header("Content-type: text/plain");

$db = get_PDO();

if( isset($_POST["name"])  ) {
    $name = strtolower ( $_POST["name"]);
    $nickname = strtoupper($name);
    if(isset($_POST["nickname"])){
        $nickname = $_POST["nickname"];
    }
    date_default_timezone_set('America/Los_Angeles');
    $time = date('y-m-d H:i:s');

    try {
        $sql = "INSERT INTO Pokedex(name, nickname,  datefound) "
            . "VALUES (:name, :nickname, :datefound);";
        $stmt = $db->prepare($sql);
        $params = array("name" => $name,
                    "nickname" => $nickname,
                    "datefound" => $time );
        $stmt->execute($params);
        $sucess = ($_POST["name"]).' added to your Pokedex!';
        $sucess = sucess();
    }
    catch (PDOException $ex)  {
        $error = error_msg(($_POST["name"]).' already found.');
    }
}
else{
    $error = error_msg('Missing name parameter.');
}

?>
<?php

# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program adds suporting funtions to all the files 

    ##Returns the PDO connecting with the object
  function get_PDO() {
    # Variables for connections to the database.
    # TODO: Replace with your server (e.g. MAMP) variables as shown in lecture on Friday.
    $host = "localhost";     # fill in with server name (e.g. localhost)
    $port = "8889";      # fill in with a port if necessary (will be different mac/pc)
    $user = "root";     # fill in with user name
    $password = "root"; # fill in with password (will be different mac/pc)
    $dbname = "hw5db";   # fill in with db name containing your SQL tables

    # Make a data source string that will be used in creating the PDO object
    $ds = "mysql:host={$host}:{$port};dbname={$dbname};charset=utf8";
    
    try {
      $db = new PDO($ds, $user, $password);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $db;
    }
    catch (PDOException $ex) {
      header("HTTP/1.1 503 Service Unavailable");
      header("Content-Type: text/plain");
      die("Cannot connect to the database. Please try again later.");
    }
  }

  #Prints the error msg
  function error_msg($error ){
    header("HTTP/1.1 400 Error");
    header("Content-Type: text/plain");
    print(json_encode(array('error' => $error )));
  }

  #fetches already exisiting $param as name
  function already_exists($param){
    $db = get_PDO();
    $name = strtolower($param);
    $stmt = $db->prepare("SELECT * FROM Pokedex WHERE name=?");
    $stmt->execute([$name]); 
    return $stmt->fetch();
  }

  #modifies and prints the success msg
  function sucess($msg){
    print(json_encode(array('success' => $msg  )));
  }



  
?>
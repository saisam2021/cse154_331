<?php
include 'common.php';

header("Content-type: text/plain");
# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program returns the message whether one has been deleted or not
# it take in two parameters: one mode is the name and the other is mode
# name removes the specific pokemon
# mode when passed in move all removes all 

$db = get_PDO();



if( isset($_POST["name"])  ) {
    $result = already_exists($_POST["name"]);
    if($result){
        $name = strtolower($_POST["name"]);
        $stmt = $db->prepare("DELETE FROM Pokedex WHERE name=?");
        $stmt->execute([$name]); 
        $sucess = ($_POST["name"]).' removed from your Pokedex!';
        print(json_encode(array('success' => $sucess  )));
    }
    else{
        $error = error_msg(($_POST["name"]).' not found in your Pokedex.');
    }
}
else if ( isset($_POST["mode"])) {
    if($_POST["mode"] === "removeall"){
        $stmt = $db -> prepare("DELETE FROM Pokedex");
        $stmt -> execute(); 
        $sucess = "All Pokemon removed from your Pokedex!";
        print(json_encode(array('success' => $sucess  )));
    }
    else{
        
        $error = error_msg('Unknown mode '.$_POST["mode"].'.');
    }
}
else{
    $error = error_msg('Missing name or mode parameter.');
}











?>
<?php

# CSE 154
# Spring 2019
# Saisamrit Surbehera
# Section AL

# This program returns the current list of pokemon the user has

header("Content-type: text/plain");
include 'common.php';

$db = get_PDO();

$rows = $db-> query("SELECT * FROM Pokedex;");
$row = $rows-> fetchAll(PDO::FETCH_ASSOC);


$result = array();

foreach( $row as $pokemon){
    array_push($result, array('name' => $pokemon["name"] , 'nickname' =>
    $pokemon["nickname"] , 'datefound' => $pokemon["datefound"] ));
}

print(json_encode(array('pokemon' => $result )));

?>
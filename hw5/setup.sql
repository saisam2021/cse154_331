USE hw5db;

CREATE TABLE Pokedex (
    name VARCHAR(30),
    nickname VARCHAR(30),
    datefound DATETIME, 
    PRIMARY KEY (name)
);
/*
  Name: Saisamrit Surbehera
  Section : AL

  This is my index.js file. It  handle all the events by the user.
*/

(function() {
  "use strict";

  /*This is the base url to get the data from php file.*/
  const URL_BASE = "getData.php";

  window.addEventListener("load", initialize);

  /**
   * This runs at the start of the page load.
   * This allows the dropdown to be populated and the button to respond once the user clicks it.
   */
  function initialize() {

    $("select").addEventListener("click", selectView);
        $("add").onsubmit = function() {
            submitData();
            return false;
        };
        $("show").onsubmit = function() {
            //findShows();
            return false;
        };
        
  }


    function selectView() {
        let type = qs('input[name="type"]:checked').value;
        if (type === "add") {
            $("add").classList.remove("hidden");
            $("show").classList.add("hidden");
        } 
        else  {
            $("add").classList.add("hidden");
            $("show").classList.remove("hidden");
        }
    }


    function submitData(){
        let showInfo = new FormData();
        showInfo.append("City", qs('input[name="City"]').value);
        console.log(qs('input[name="City"]').value);
        showInfo.append("Country", qs('input[name="Country"]').value);
        console.log(qs('input[name="Country"]').value);
        showInfo.append("Airline", qs('input[name="Airline"]').value);
        console.log(qs('input[name="Airline"]').value);
        showInfo.append("Continent", qs('input[name="Region"]').value);
        console.log(qs('input[name="Region"]').value);
        $("add").reset();
        fetch(URL_BASE, { method : "POST", body : showInfo })
            //.then(checkStatus)
            //.then(confirmAdd)
            //.catch(showError);
    }

    function confirmAdd(message) {
        removeChildren($("results"));
        $("results").innerText = message;
    }


  /**
   * This recieves a list of countries and populates the dropdown menu with the all the name of
   * the countries
   */
  function getreplace() {
    let url = URL_BASE + "?state=all";
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(fillDropDown)
      .catch(handleError);
  }




  /**
   * This lets the user know when an error occured during the attempt to recieve the list of
   * states
   */
  function handleError() {
    let message = document.createElement("p");
    message.innerText = "Due To Global warming and rising sea levels, The Airport has disappeared";
    qs("fieldset").appendChild(message);
  }

  /* ------------------------------ Helper Functions (from slide) ------------------------------ */


  function $(id) {
    return document.getElementById(id);
  }

  /**
   * Returns the first element that matches the given CSS selector.
   * @param {string} query - CSS query selector.
   * @returns {object} The first DOM object matching the query.
   */
  function qs(query) {
    return document.querySelector(query);
  }

  /**
   * Helper function to return the response's result text if successful, otherwise
   * returns the rejected Promise result with an error status and corresponding text
   * @param {object} response - response to check for success/error
   * @returns {object} - valid result text if response was successful, otherwise rejected
   *                     Promise result
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300 || response.status == 0) {
      return response.text();
    } else {
      return Promise.reject(new Error(response.status + ": " + response.statusText));
    }
  }
})();

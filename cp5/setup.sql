create database country;
use country;

CREATE TABLE AirportVisited (
    id INT(20),
    City VARCHAR(255),
    Country VARCHAR(255),
    Airline VARCHAR(255), 
    Continent VARCHAR(255)
);

INSERT INTO AirportVisited(id, name, email, student_id, time, question)
VALUES (1, 'Chennai' , 'India', 'Air India', 'Asia'),
        (2, 'Hyderabad' , 'India', 'Indigo', 'Asia'),
        (3, 'Mumbai' , 'India', 'Vistara', 'Asia'),
        (4, 'Calcutta' , 'India', 'Indigo', 'Asia'),
        (5, 'Bhubaneshwar' , 'India', 'Indigo', 'Asia'),
        (6, 'Bangalore' , 'India', 'Vistara', 'Asia'),
        (7, 'New India' , 'India', 'Indigo', 'Asia'),
        (8, 'Doha' , 'Qatar', 'Qatar Airways', 'Middle East'),
        (9, 'Dubai' , 'Emirates', 'UAE', 'Middle East'),
        (10, 'Abu Dhabi' , 'Ethiad Airways', 'UAE', 'Middle East'),
        (11, 'Frankfurt' , 'Lufthansa', 'Germany', ' Western Europe'),
        (12, 'London' , 'British Airways', 'UK', ' Western Europe'),
        (13, 'Seattle' , 'American Airways', 'USA', 'North America'),
        (14, 'New York' , 'JetBlue', 'USA', 'North America'),
        (15, 'Chicago' , 'British Airways', 'USA', 'North America');


# getData API Documentation
*Provides the user with all the data for state capitals and list of all states*

## Get A list of all States
**Request Format:**  getData.php?name=all

**Request Type:** GET

**Returned Data Format**: Plain Text

**Description:** Returns a lost of all the States and their state code

**Example Request:** getData.php?name=all

**Example Response:**


```
{
  Alabama	AL
  Alaska	AK
  Arizona	AZ
  Arkansas	AR
  California	CA
  Colorado	CO
  Connecticut	CT
}
```

**Error Handling:**
- If missing the 'all', , it will 400 error with a helpful message: `Error: Please all as a parameter.`

## Get The capital
**Request Format:** getData.php?data=true

**Request Type**: GET

**Returned Data Format**: JSON

**Description:** Given the valid parameter returns a JSON object with the all the data of the states

**Example Request:** getData.php?data=true

**Example Response:**


```
{"AL":"Montgomery\r",
"AK":"Juneau\r",
"AZ":"Phoenix\r",
"AR":"LittleRock\r",
"CA":"Sacramento\r",
"CO":"Denver\r"
```

**Error Handling:**
Throws a 400 error if you don't pass a valid result

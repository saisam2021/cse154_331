/*
  Name: Saisamrit Surbehera
  Section : AL
  Date: May4

  This is my index.js file. It interactively play the flappy bird Game
	and handle all the events by the user.
*/


(function() {
  "use strict";

  /*This is the base url to get the data from countries.*/
  const URL_BASE = "https://restcountries.eu/rest/v2";
  let countrySelected;
  /*This keeps track of the country the user selects.*/

  window.addEventListener("load", initialize);

  /**
   * This runs at the start of the page load.
   * This allows the dropdown to be populated and the button to respond once the user clicks it.
   */
  function initialize() {
    getCountry();
    qs("button").addEventListener("click", getInfo);
  }

  /**
   * This recieves a list of countries and populates the dropdown menu with the all the name of
   * the countries
   */
  function getCountry() {
    let url = URL_BASE + "/all";
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(fillDropDown)
      .catch(handleError);
  }

  /**
  * Fills the dropdown menu with the list of restcountrie
  * @param {Object} data - contains the data of countries
  */
  function fillDropDown(data) {
    for (let i = 0; i < data.length; i++) {
      let newOption = document.createElement("option");
      newOption.value = data[i].name;
      newOption.innerText = data[i].name;
      qs("select").appendChild(newOption);
    }
  }

  /** Traverses through the whole data set to retrive coutires data
  */
  function getInfo(){
    qs("div").innerHTML="";
    countrySelected = qs("select").value;
    let url = URL_BASE +"/name/"+ countrySelected + "/?fullText=true";
    fetch(url, {mode : "cors"})
      .then(checkStatus)
      .then(JSON.parse)
      .then(showDetails);
  }


  /** Finds the flag of the coutry
  * @param {Object} dataJSON - contains the data of countries
  */
  function showDetails(dataJSON){
    let data = dataJSON[0];
    let newCountry = document.createElement("div");
    let cardImg  = document.createElement("img");
    cardImg.src = data.flag;
    cardImg.alt = "Flag";
    newCountry.appendChild(cardImg);
    qs("div").appendChild(newCountry);
  }


  /**
   * This lets the user know when an error occured during the attempt to recieve the list of
   * countries
   */
  function handleError() {
    let message = document.createElement("p");
    message.innerText = "Due To Global warming, The country has disappeared";
    qs("fieldset").appendChild(message);
  }

  /* ------------------------------ Helper Functions (from slide) ------------------------------ */

  /**
   * Returns the first element that matches the given CSS selector.
   * @param {string} query - CSS query selector.
   * @returns {object} The first DOM object matching the query.
   */
  function qs(query) {
    return document.querySelector(query);
  }

  /**
   * Helper function to return the response's result text if successful, otherwise
   * returns the rejected Promise result with an error status and corresponding text
   * @param {object} response - response to check for success/error
   * @returns {object} - valid result text if response was successful, otherwise rejected
   *                     Promise result
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300 || response.status == 0) {
      return response.text();
    } else {
      return Promise.reject(new Error(response.status + ": " + response.statusText));
    }
  }
})();
